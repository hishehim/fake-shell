/*
AUTHOR: Shankun Lin (Mike)
Date: May 24, 2016
Description:
    A demo shell written in C. It accepts few default actions:
    ctrl+D: exits the shell.
    'exit': exits the shell.
    'cd': changes working directory.
    Shell is can execute program and other non-shell built-it commands.
*/

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pwd.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/wait.h>
#include <signal.h>

#define MAX_BUF_SIZE 4096
#define EXIT_CODE -9

char* getUsrNam();
int launch(const char argc, char** const argv);
int execute(char** const argv);
int parser(char *line, int size, char **argv, int *argc);
void onExeSigInt(int signo);


static pid_t childPid = -1;
static char *username;
static char cwd[MAX_BUF_SIZE];


int main(void) {
    int i, inputLen, argc;
    char input[MAX_BUF_SIZE];
    char* argv[MAX_BUF_SIZE];

    username = getUsrNam();
    if (getcwd(cwd, MAX_BUF_SIZE) == NULL) {
        perror("Error getcwd()");
        exit(EXIT_FAILURE);
    }

    while (1) {
        printf("%s:%s$ ", username, cwd);
        errno = 0;
        if (fgets(input, MAX_BUF_SIZE, stdin) == NULL) {
            if (errno == 0) {
                putchar('\n');
                return 0;
            } else {
                perror("Read line via fgets()");
                exit(EXIT_FAILURE);
            }
        }
        inputLen = strlen(input);
        // fgets() retains '\n', replace with terminator
        (input[inputLen-1] == '\n') ? input[inputLen--] = '\0' : 0;
        if (parser(input, inputLen, argv, &argc) < 1) {
            // nothing parsed, restart loop
            continue;
        }
        if (launch(argc, argv) == EXIT_CODE) {
            break;
        }
    }
    return 0;
}


char* getUsrNam() {
    struct passwd *pwd = getpwuid(getuid());
    return pwd->pw_name;
}

int parser(char *line, int size, char **argv, int *argc) {
    int tokenCnt = 0;
    int i = 0;
    char *start, *end, *cur;

    /* trim the front of the line */
    start = line;
    while (*start == ' ') {
        start++;
    }
    
    if (start == (line + size)) {
        // entire line is space
        return 0;
    }

    /* trim the end of the line */
    end = (line + size);
    while (*(end - 1) == ' ') {
        end--;
    }
    *end = '\0';


    cur = start;
    tokenCnt = 1;
    while(cur != end) {
        if (*cur == ' ' || *cur == '\t') {
            cur++;
            while ((cur != end) && ((*cur == ' ') || (*cur == '\t')))  {
                cur++;
            }
            tokenCnt++;
            continue;
        }
        cur++;
    }
    *argc = tokenCnt;
    i = 0;
    cur = start;
    while (cur != end) {
        if (*cur != ' ' && *cur != '\t') {
            argv[i] = cur;
            i++;
            while ((cur != end) && ((*cur != ' ') && (*cur != '\t')) ) {
                cur++;
            }
            continue;
        } else {
            *cur = '\0';
        }
        cur++;
    }
    argv[tokenCnt] = NULL;
    return tokenCnt;
}

int launch(const char argc, char** const argv) {
    if (strcmp(argv[0], "cd") == 0) {
        if (argc >= 2) {
            if (chdir(argv[1]) < 0) {
                fprintf(stderr, "%s: %s\n", argv[1], strerror(errno));
            } else {
                if (getcwd(cwd, MAX_BUF_SIZE) == NULL) {
                    perror("Error getcwd()");
                    exit(EXIT_FAILURE);
                }
            }
        }
    } else if (strcmp(argv[0], "exit") == 0) {
        return EXIT_CODE;
    } else if (strcmp(argv[0], "help") == 0) {
        printf("These is a fake shell emulating a shell.\n");
        printf("Accepted most standard commands.\n");
    }
    else {
        return execute(argv);
    }
    return 0;
}


int execute(char** const argv) {
    pid_t pid;
    int status, i;
    sigset_t block;
    struct sigaction act, oact;

    /**/
    sigemptyset(&block);
    act.sa_handler = onExeSigInt;
    act.sa_mask = block;
    act.sa_flags = SA_RESTART;
    sigaction(SIGINT, &act, &oact);

    if ((pid = fork()) < 0) {
        fprintf(stderr, "Could not start program: %s\n", strerror(errno));
        return -1;
    }

    if (pid == 0) {
        if (execvp(argv[0], argv) < 0) {
            if ((strlen(argv[0]) > 2) && ((argv[0][0] == '.') && (argv[0][1] == '/' ))) {
                fprintf(stderr, "%s: %s\n", argv[0], strerror(errno));
            } else {
                fprintf(stderr, "%s: Command not found\n", argv[0]);
            }
            exit(EXIT_FAILURE);
        }
    }
    childPid = pid;
    
    do {
        waitpid(pid, &status, WUNTRACED);
    } while (!WIFEXITED(status) && !WIFSIGNALED(status));
    
    // put old handler back
    childPid = -1;
    sigaction(SIGINT, &oact, NULL);
    return 0;
}

void onExeSigInt(int signo) {
    int status;
    kill(childPid, SIGINT);
    do {
        waitpid(childPid, &status, WUNTRACED);
    } while (!WIFEXITED(status) && !WIFSIGNALED(status));
    putchar('\n');
}