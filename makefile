#NAME: Shankun Lin
#Date: May 20, 2016
#FINAL PROJECT: SHELL
SRC = src
PROG1_MAIN = myshell
PROG1_NAME = myshell
PROG1_DEP = $(SRC)/$(PROG1_MAIN).c

PROG_DEMO = helloworld
PROG_DEMO_DEP = $(SRC)/$(PROG_DEMO).c

all: $(PROG1_NAME)

$(PROG1_NAME): $(PROG1_DEP) $(PROG_DEMO)
	gcc -g $(PROG1_DEP) -o $@

$(PROG_DEMO): $(PROG1_DEP)
	gcc $(PROG_DEMO_DEP) -o $@