Author: Shankun Lin
Date: May 20, 2016
CS:493 UNIX-2
FINAL PROJECT: SHELL

---------------
COMPILE OPTIONS
---------------
run:
    make

Programs Included:
    myshell
    helloworld

------------
DESCRIPTIONS
------------
NAME: myshell
RUN: ./myshell
DESCRIPTION:
    A demo shell written in C. It accepts few default actions:
    ctrl+D: exits the shell.
    'exit': exits the shell.
    'cd': changes working directory.
    Shell is can execute program and other non-shell built-it commands.

NAME: helloworld
RUN: ./helloworld
DESCRIPTION:
    Prints "HELLO WORLD" and sleeps for ten seconds
    Used as quick demo of myshell handling SIGINT